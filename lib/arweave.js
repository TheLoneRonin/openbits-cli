import chalk from 'chalk';
import CLI from 'clui';
import Sentencer from 'sentencer';
import Arweave from 'arweave/node';
import { readContract } from 'smartweave';
import {
  table,
} from 'table';

import configStore from './config-store';
import {
  userKey,
  isNodePackage,
} from './checks';
import {
  installNodePackage,
  packNodePackage,
} from './npm';
import {
  askForOpenBitPayment,
  askForTargetProfit,
  askForAllowInvestments,
  askForConfirmPublication,
} from './inquirer';
import {
  getPackageJson,
  // getCurrentDirectory,
  // readFile,
} from './files';
import {
  createCbvacPstInitState,
  createCbvacInvestmentLevelsTable,
  createCbvacInfoTable,
} from './cbvac-pst';
import {
  openBitCreatePSTTransaction,
  publishOpenBitTransaction,
} from './transactions';
import latestVersion from './semver';

// init the arweave network
export const getArweave = () => {
  const arweaveNode = Arweave.init({
    host: 'arweave.net',
    port: 443,
    protocol: 'https',
    timeout: 20000,
    logging: false,
  });
  return arweaveNode;
};

// logs the user into the arweave
export const loginArweave = (ak) => {
  configStore().set('arweave-key', ak);
};

// logs-out the user from the arweave
export const logoutArweave = () => {
  configStore().clear();
};

export const getUserAddress = async () => {
  const userAddress = await getArweave().wallets.jwkToAddress(JSON.parse(userKey()));
  return userAddress;
};

export const getUserBalance = async () => {
  const userAddress = await getUserAddress();
  const userBalance = await getArweave().wallets.getBalance(userAddress);
  const userBalanceAr = await getArweave().ar.winstonToAr(userBalance);
  return userBalanceAr;
};

// list the OpenBits saved in the registry
export const listOpenBits = async () => {
  const { Spinner } = CLI;
  const status = new Spinner('Loading the OpenBits registry, please wait...');
  status.start();
  const rs = await readContract(getArweave(), configStore().get('arweaveOpenBitsRegistryAddress'));
  console.log(chalk.yellow('\nREGISTERED OPENBITS:\n-------------------------\n'));
  // get node packages
  const { OpenBits } = rs;

  Object.values(OpenBits.nodePackages).forEach((v) => {
    console.log(chalk.blue(`--- ${v.name}@${v.version}`));
  });
  status.stop();
  console.log(chalk.green('\nTo install one of them run:\n'));
  console.log(
    `\t${chalk.bgBlue('openbits install openbit-name[@openbit-version]\n')}`,
  );
  return OpenBits;
};

export const createInstallTransaction = async (pstId, target) => {
  // the arweave node
  const arweave = await getArweave();
  // the user key
  const key = JSON.parse(userKey());

  // the price for installing an OpenBit
  const quantity = arweave.ar.arToWinston(configStore().get('installationFee'));

  // create the transaction
  const transaction = await arweave.createTransaction({
    target,
    quantity,
  }, key);

  // add tags to the transaction in order to make it a contract call
  transaction.addTag('dApp', 'OpenBits');
  transaction.addTag('type', 'node-package');
  transaction.addTag('action', 'install');
  transaction.addTag('App-Name', 'SmartWeaveAction');
  transaction.addTag('App-Version', '0.3.0');
  transaction.addTag('Contract', pstId);
  transaction.addTag('Input', JSON.stringify({
    function: 'install',
  }));

  // sign the transaction
  await arweave.transactions.sign(transaction, key);
  return transaction;
};

// get OpenBit status
export const getPublishedOpenBitStatus = async (openBitCompleteName) => {
  const publishedByUser = configStore().get('userPublications');
  publishedByUser.forEach(async (openbit) => {
    if (openbit.completeName === openBitCompleteName) {
      const statusPst = await getArweave().transactions.getStatus(openbit.pstId);
      const statusOpenBitId = await getArweave().transactions.getStatus(openbit.openBitId);
      if (statusPst.status === 202 && statusOpenBitId.status === 202) {
        console.log(chalk.red(`The publication of ${openBitCompleteName} is still pending`));
        return false;
      }
      console.log(chalk.green(`${openBitCompleteName} was succesfully published!`));
      return true;
    }
    return true;
  });
};

// install an OpenBit
export const installOpenBit = async (name, saveDev) => {
  // check if the user is logged in
  if (!userKey()) {
    console.log(chalk.red('\nI\'m so sorry 😓! You must be logged in to install an OpenBit. Please run:\n'));
    console.log('\t');
    console.log(chalk.bgBlue('openbits login <path-to-your-arweave-key-store>\n'));
    process.exit(0);
  }
  // retrieve OpenBits from the registry
  const { Spinner } = CLI;
  const status = new Spinner('Loading the OpenBits registry, please wait...');
  status.start();
  const rs = await readContract(getArweave(), configStore().get('arweaveOpenBitsRegistryAddress'));

  const { OpenBits } = rs;

  // if the user want to install a specific version, get that one
  let openbit = OpenBits.nodePackages[name];
  // else check if the package exists and install the latest version
  if (!openbit) {
    const { nodePackages } = OpenBits;
    const versions = [];
    Object.keys(nodePackages).forEach((k) => {
      if (k.includes(name)) {
        versions.push(nodePackages[k].version);
      }
    });
    openbit = OpenBits.nodePackages[`${name}@${latestVersion(versions)}`];
  }
  if (!openbit) {
    console.log(chalk.yellow('\n\nThis package is not available on OpenBits, falling back to NPM\n'));
    await installNodePackage(name, saveDev);
    process.exit(0);
  }

  // check if the OpenBit is compatible with the project type
  switch (openbit.type) {
    case 'node-package':
      if (!isNodePackage()) {
        console.log(chalk.red('\n🤔 The OpenBit you are trying to install is a node package. But it seems that currently you are not in a folder that contains a package.json. 🤔\n'));
        console.log(chalk.red('🍺 If you are drunk, consider having another beer before trying again. If you are not drunk, consider having a first beer before trying again. 🍺\n'));
        process.exit(0);
      }
      break;
    default:
      console.log(chalk.green('\nThe OpenBit that you are trying to install is not recognized.\n'));
      process.exit(0);
  }

  status.stop();

  const statusPst = new Spinner('Retrieving the status of the OpenBit');
  statusPst.start();
  const os = await readContract(getArweave(), openbit.pstId);

  statusPst.stop();

  const installTransaction = await createInstallTransaction(openbit.pstId, os.currentlyIsPaying);

  const statusOpenBitsPst = new Spinner('Retrieving the OpenBits contract');
  statusOpenBitsPst.start();

  const obs = await readContract(getArweave(), configStore().get('arweaveOpenBitsCBVACPSTState'));

  const openbitsFeeTransaction = await createInstallTransaction(
    configStore().get('arweaveOpenBitsCBVACPSTState'),
    obs.currentlyIsPaying,
  );
  statusOpenBitsPst.stop();

  // get the user balance

  const statusCheckBalance = new Spinner('Checking your balance');
  statusCheckBalance.start();
  const userBalance = await getUserBalance();
  statusCheckBalance.stop();

  // compute the total amount for the installation
  const totalCost = (configStore().get('installationFee') * 2)
    + Number(getArweave().ar.winstonToAr(installTransaction.reward))
    + Number(getArweave().ar.winstonToAr(openbitsFeeTransaction.reward));

  // get the openbit URL
  const openBitURL = `https://www.arweave.net/${openbit.dataId}`;

  if (userBalance < totalCost) {
    console.log(chalk.green('❤️  You don\'t have enough AR to contribute a donation to the OpenBit MULTIVERSE! It will be installed anyway because OpenBits loves you anyway! ❤️\n'));
    if (openbit.type === 'node-package') {
      await installNodePackage(name, saveDev);
      process.exit(0);
    }
  }

  // create a random word for confirmation
  const randWord = Sentencer.make('{{ adjective }}').toUpperCase();

  console.log(chalk.green(`\n\n💰 Hey! Installing this OpenBit will cost you ${totalCost}  AR! 💰\n`));
  console.log(chalk.green(`You have ${userBalance} AR. So if you pay your balance after that will be ${userBalance - totalCost} AR\n`));
  console.log(chalk.bgBlue(`💸 If you want to pay write ${randWord} 💸\n`));
  console.log(chalk.green('❤️ Otherwise just write anything else to cancel the installation ❤️\n'));

  const resp = await askForOpenBitPayment(randWord, totalCost);

  if (resp.payOpenBit === randWord) {
    console.log(chalk.bgBlue('\n❤️ THANK YOU OPENBITS AND THE MULTIVERSE WILL ALWAYS BE GRATEFUL FOR YOUR CONTRIBUTION! ❤️\n'));
    getArweave().transactions.post(installTransaction);
    console.log(chalk.green('Sent 0.01 AR to the user that is currently being paid when this OpenBit is installed.\n'));
    console.log(chalk.green(`The transaction id is: ${installTransaction.id} \n`));
    getArweave().transactions.post(openbitsFeeTransaction);
    console.log(chalk.green('Sent 0.01 AR to the user that is currently being paid when ANY OpenBit is installed.\n'));
    console.log(chalk.green(`The transaction id is ${openbitsFeeTransaction.id} \n`));
    console.log(chalk.bgBlue('❤️ OPENBITS WILL NOT WAIT FOR THE CONFIRMATION OF THESE TRANSACTIONS, BECAUSE WE TRUST YOU ❤️\n'));
  } else {
    console.log(chalk.bgBlue('\n❤️ IT DOESN\'T MATTER. THE OPENBIT WILL BE INSTALLED ANYWAY BECAUSE OPENBITS LOVES YOU! ❤️\n'));
  }

  // the open bit is a node package
  if (openbit.type === 'node-package') {
    await installNodePackage(name, saveDev);
  }

  process.exit(0);
};

export const publishOpenBit = async () => {
  // check if the user is logged in
  if (!userKey()) {
    console.log(chalk.red('\nI\'m so sorry 😓! You must be logged in to install an OpenBit. Please run:\n'));
    console.log('\t');
    console.log(chalk.bgBlue('openbits login <path-to-your-arweave-key-store>\n'));
    process.exit(0);
  }

  // some useful thing
  const { Spinner } = CLI;
  let spinner = new Spinner('Packing your OpenBit');

  // the openbit pack
  let openbit = null;

  // get the name and version of the OpenBit that the user is trying to publish
  const packageJson = await getPackageJson();
  const completeName = `${packageJson.name}@${packageJson.version}`;

  // check what kind of OpenBit is
  if (isNodePackage()) {
    spinner.start();
    // await packNodePackage();
    // const packFile = readFile(
    // `${getCurrentDirectory()}/${packageJson.name}-${packageJson.version}.tgz`);
    openbit = await packNodePackage();
    spinner.stop();
  }
  if (!openbit) {
    console.log(chalk.red('\n😢 OpenBits does not yet support this kind of packages. Apologies for that. 😢\n'));
    process.exit(0);
  }

  // check if there is another packge already installed with the same name and version
  spinner = new Spinner('Loading the OpenBits registry, please wait...');
  spinner.start();
  const rs = await readContract(getArweave(), configStore().get('arweaveOpenBitsRegistryAddress'));
  const { nodePackages } = rs.OpenBits;

  spinner.stop();

  if (nodePackages[completeName]) {
    console.log(chalk.red('\n😢 Another OpenBit with the same name and version was already published 😢\n'));
    process.exit(0);
  }

  // always say some sweet word to users!!!!!
  console.log(chalk.bgBlue(`\n❤️ GREAT! YOUR OPENBIT ${completeName} SEEMS AMAZING! ❤️\n`));

  // ask for the target profit
  const res = await askForTargetProfit();
  const targetProfit = Number(res.targetProfit);

  // create the cbvac pst for the OpenBit
  const cbvacPstInitState = createCbvacPstInitState(
    completeName,
    [await getUserAddress()],
    targetProfit,
  );

  console.log(createCbvacInfoTable(cbvacPstInitState));
  console.log(chalk.green(`💰 You can allow 4 investors to buy shares of your ${completeName}: 💰\n`));

  console.log(createCbvacInvestmentLevelsTable(cbvacPstInitState));

  // ask users if they want to allow investments
  const resI = await askForAllowInvestments();
  if (!resI.allowInvestments) {
    cbvacPstInitState.sharesAvailableForInvestors.levels[0].available = false;
    cbvacPstInitState.sharesAvailableForInvestors.levels[1].available = false;
    cbvacPstInitState.sharesAvailableForInvestors.levels[2].available = false;
    cbvacPstInitState.sharesAvailableForInvestors.levels[3].available = false;
  }

  console.log('\n');

  // create the transaction for publishing the OpenBits CBVAC PST
  spinner = new Spinner('Creating the transaction for publishing the Profit Share Token of your OpenBit');
  spinner.start();
  const pstTransaction = await openBitCreatePSTTransaction(
    getArweave(),
    JSON.parse(userKey()),
    cbvacPstInitState,
  );
  spinner.stop();

  // create the transaction for publishing the OpenBit in the registry
  // get the user that is currently being payed by OpenBits
  spinner = new Spinner('Getting the address of the user that is currently being payed by OpenBits');
  spinner.start();
  // ----- TO DO VERY IMPORTANT ----- MUST BE INVESTIGATED BECAUSE SOON OR LATER
  // THE OPENBITS REGISTRY OWNER MUST CHANGE ACCORDING TO THE MULTIVERSE USER
  const openBitCBVACState = await readContract(getArweave(), configStore().get('arweaveOpenBitsCBVACPSTState'));
  const publishTarget = openBitCBVACState.currentlyIsPaying;
  spinner.stop();

  spinner = new Spinner('Creating the public transaction');
  spinner.start();
  const publishTransaction = await publishOpenBitTransaction(
    getArweave(),
    JSON.parse(userKey()),
    openbit.buffer,
    await getArweave().ar.arToWinston(configStore().get('publicationFee')),
    publishTarget,
    packageJson.name,
    packageJson.version,
    pstTransaction.id,
  );
  spinner.stop();

  // get the user balance
  spinner = new Spinner('Checking your balance');
  spinner.start();
  const userBalance = await getUserBalance();
  spinner.stop();

  // compute the total amount for the installation
  const totalCost = configStore().get('publicationFee')
    + Number(getArweave().ar.winstonToAr(pstTransaction.reward))
    + Number(getArweave().ar.winstonToAr(publishTransaction.reward));

  // table for publication fee summary
  const data = [
    [
      chalk.bold('Fee for publication owed to OpenBits'),
      chalk.bold('Fee for transaction owed to Arweave'),
      chalk.bold('Total Cost'),
      chalk.bold('Your balance'),
      chalk.bold('Your balance after publication'),
    ],
    [
      `${configStore().get('publicationFee')} AR`,
      `${Number(getArweave().ar.winstonToAr(pstTransaction.reward)) + Number(getArweave().ar.winstonToAr(publishTransaction.reward))} AR`,
      `${totalCost} AR`,
      `${userBalance} AR`,
      `${userBalance - totalCost} AR`,
    ],
  ];
  console.log(table(data));

  if (userBalance < totalCost) {
    console.log(chalk.red('😢 You don\'t have enough AR to publish this OpenBit! 😢\n'));
    console.log(chalk.blue('🦄 Unfortunately OpenBits can\'t help you about that! However, we still believe your project is a Unicorn! 🦄\n'));
    process.exit(0);
  }

  // create a random word for confirmation
  const randWord = Sentencer.make('{{ adjective }}').toUpperCase();

  console.log(chalk.green(`\n\n💰 ALMOST DONE! The publication of this OpenBit will cost ${totalCost} AR! 💰\n`));
  console.log(chalk.green(`You have ${userBalance} AR. After publishing this OpenBit your balance will be ${userBalance - totalCost} AR\n`));
  console.log(chalk.bgBlue(`💸 If you want to continue write ${randWord} 💸\n`));
  console.log(chalk.green('❤️ Otherwise just write anything else. In that case, nothing will happen but OpenBits will love you anyway! ❤️\n'));

  const resp = await askForConfirmPublication(randWord, totalCost);

  if (resp.confirmPublication === randWord) {
    // copy the user publications array
    const up = configStore().get('userPublications');
    up.push({
      completeName,
      pstId: pstTransaction.id,
      openBitId: publishTransaction.id,
    });

    configStore().set('userPublications', up);
    // send transactions
    spinner = new Spinner(`Publishing ${completeName}`);
    spinner.start();
    await getArweave().transactions.post(pstTransaction);
    await getArweave().transactions.post(publishTransaction);
    spinner.stop();
    console.log(chalk.bgBlue('\n❤️ This is amazing. Your package will be published soon on OpenBits! The followings is the arweave transaction id: ❤️\n'));
    console.log(chalk.green(`The OpenBit PST creation transaction id is: ${pstTransaction.id}\n`));
    console.log(chalk.green(`The OpenBit publication transaction id is: ${publishTransaction.id}\n`));
    console.log(chalk.green(`You can check the publication status of ${completeName} by running:\n`));
    console.log(chalk.bgBlue(`openbits published-status ${completeName}\n`));
    process.exit(1);
  } else {
    console.log(chalk.bgBlue('\n❤️ IT DOESN\'T MATTER. AS ALREADY TOLD, OPENBITS LOVES YOU ANYWAY! ❤️\n'));
  }
  // compute the total amount of AR that are needed for publication
};
