import fs from 'fs';
import path from 'path';

export const getCurrentDirectoryBase = () => (
  path.basename(process.cwd())
);

export const getCurrentDirectory = () => (
  process.cwd()
);

export const directoryExists = (filePath) => (
  fs.existsSync(filePath)
);

export const readFile = (filePath) => (
  fs.readFileSync(filePath)
);

export const getPackageJson = async () => {
  const packageJson = JSON.parse((fs.readFileSync('package.json')).toString());
  return packageJson;
};
