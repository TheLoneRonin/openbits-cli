// const inquirer = require('inquirer');
// inquirer.registerPrompt('filePath', require('inquirer-file-path'));
import chalk from 'chalk';
import Sentencer from 'sentencer';
import {
  directoryExists,
  readFile,
} from './files';
import {
  loginArweave,
  logoutArweave,
  listOpenBits,
  installOpenBit,
  publishOpenBit,
  getPublishedOpenBitStatus,
} from './arweave';

/**
  * Login the user into the arweave. Their key-file will be permanently stored in the
  * configstore. To delete it they must run the logout command
*/
export const login = (arweaveKeyStore) => {
  if (!directoryExists(arweaveKeyStore)) {
    console.error(
      chalk.red("Your arweave key store file was not found! Are you sure that is the right path?"),
    );
  }
  const arKey = readFile(arweaveKeyStore);
  loginArweave(arKey.toString());
  console.log(
    chalk.green('You are successfully logged into OpenBits with your arweave key file\n\n'),
    chalk.bold.bgRed('WARNING\n\n'),
    chalk.bold.red('When you are done remember to run: \n\n\t'),
    chalk.bgBlue('openbits logout\n\n'),
    chalk.bold.red('If you do not, your arweave key will remain stored in this computer. Ignore this message if it\'s a personal computer 😉\n'),
  );
};

/**
 * Logout the users and delete their arweave key-file from the config store
 */
export const logout = () => {
  logoutArweave();
  console.log(
    chalk.green('Thank You!\n\n'),
    chalk.bold.bgGreen('AND REMEMBER\n\n'),
    chalk.bold.bgMagenta(`${Sentencer.make('OpenBits is {{ an_adjective }} because it allows {{ a_noun }} to donate Open Source packages to the {{ an_adjective }} MULTIVERSE.')} \n\n`),
  );
};

/**
 * Lists the OpenBits currently registerd
*/
export const list = () => {
  try {
    listOpenBits();
  } catch (err) {
    console.log(
      chalk.error(`An error occurred when trying to list OpenBits. The error was:\n${err}`),
    );
  }
};

/**
 * install the OpenBits user has requested
*/
export const install = (name, cmd) => {
  installOpenBit(name, cmd.opts().saveDev);
};

/**
 * Publish the OpenBit
*/
export const publish = () => {
  publishOpenBit();
};

/**
  Get the status of an openbit
*/
export const publishedStatus = (name) => {
  getPublishedOpenBitStatus(name);
};
