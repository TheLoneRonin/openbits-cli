import chalk from 'chalk';
import {
  table,
} from 'table';
import configStore from './config-store';

export const getInvestmentLevelsMargin = (targetProfit) => {
  const twentyOfTarget = (targetProfit * 20) / 100;
  const levelOne = (twentyOfTarget * 10) / 100;
  const levelTwo = levelOne + ((twentyOfTarget * 7) / 100);
  const levelThree = levelTwo + ((twentyOfTarget * 5) / 100);
  const levelFour = levelThree + ((twentyOfTarget * 3) / 100);
  return [
    levelOne,
    levelTwo,
    levelThree,
    levelFour,
  ];
};

export const getInvestmentLevels = (targetProfit, owner) => {
  const levelsMargin = getInvestmentLevelsMargin(targetProfit);
  const levels = [];
  [10, 7, 5, 3].forEach((v, k) => {
    const pricePerShare = (targetProfit / 100) / v || 1;
    const expectedROI = ((targetProfit / 100) * 5) - (pricePerShare * 5);
    levels.push({
      multiplier: v,
      beforeProfit: levelsMargin[k],
      amountOfShares: 5,
      pricePerShare,
      expectedROI,
      owner,
      available: false,
    });
  });
  return levels;
};

// creates the init state of a cbvac-pst to be published on OpenBits
export const createCbvacPstInitState = (
  ticker,
  owners,
  targetProfit,
) => {
  const levels = getInvestmentLevels(targetProfit, owners[0]);
  levels[0].available = true;
  levels[0].name = 'Yellow';
  levels[0].color = 'secondary-color';
  levels[0].textColor = 'white';
  levels[1].name = 'Red';
  levels[1].color = 'danger';
  levels[1].textColor = 'white';
  levels[2].name = 'Green';
  levels[2].color = 'fourth-color';
  levels[2].textColor = 'white';
  levels[3].name = 'Blue';
  levels[3].color = 'third-colo';
  levels[3].textColor = 'white';
  const template = {
    ticker,
    shares: 100,
    owners,
    investors: [],
    users: {},
    MultiverseWaitingList: {},
    currentMultiverseAccount: {},
    sharesPayedToTheMultiverse: 0,
    targetProfit,
    generatedProfit: 0,
    installationsToReleaseShares: ((targetProfit) / 100) / 0.01,
    currentlyIsPaying: owners[0],
    sharesReleaseIn: ((targetProfit) / 100) / 0.01,
    sharesAvailableForInvestors: {
      levels,
    },
    balances: {
    },
  };
  template.balances[owners[0]] = 100;
  template.balances.multiverse = 0;
  return template;
};

export const createCbvacInvestmentLevelsTable = (cbvac) => {
  const data = [
    [
      chalk.bold('Investment Name'),
      chalk.bold('Available only before you earn'),
      chalk.bold('Number of shares that you will sell'),
      chalk.bold('Price for each share that you will sell to the investor'),
      chalk.bold('Total price that the investor will pay to you'),
      chalk.bold('Expected ROI for the investor'),
    ],
  ];
  cbvac.sharesAvailableForInvestors.levels.forEach((level) => {
    data.push([
      (() => {
        switch (level.name) {
          case 'Yellow':
            return chalk.bgYellow(level.name);
          case 'Red':
            return chalk.bgRed(level.name);
          case 'Green':
            return chalk.bgGreen(level.name);
          case 'Blue':
            return chalk.bgBlue(level.name);
          default:
            return chalk.bgYellow(level.name);
        }
      })(),
      `${level.beforeProfit} AR`,
      level.amountOfShares,
      `${level.pricePerShare} AR`,
      `${(Number(level.pricePerShare) * Number(level.amountOfShares))} AR`,
      `${level.expectedROI} AR`,
    ]);
  });
  const output = table(data);
  return output;
};

export const createCbvacInfoTable = (cbvac) => {
  const data = [
    [
      chalk.bold('Your profit target'),
      chalk.bold('Number of Shares'),
      chalk.bold('Price per Share'),
      chalk.bold('Number of installations to reach your target profit'),
      chalk.bold('Number of installations to release a share to the MULTIVERSE'),
    ],
    [
      cbvac.targetProfit,
      100,
      Number(cbvac.targetProfit) / 100,
      Number(cbvac.targetProfit) / configStore().get('installationFee'),
      (Number(cbvac.targetProfit) / 100) / configStore().get('installationFee'),
    ],
  ];
  const output = table(data);
  return output;
};
