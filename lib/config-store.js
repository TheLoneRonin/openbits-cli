import Configstore from 'configstore';
import packageJson from '../package.json';

// init the configstore if it does not exist
const configStore = () => {
  const openBitsArweaveConfig = new Configstore(packageJson.name, {
    arweaveOpenBitsRegistryAddress: 'S3a-4VByScX0vtjmh5oZPs8kHTNn-8UPFVOkm2RprDc',
    arweaveOpenBitsCBVACPSTState: 'dpRwKqXGO234CgHp5i_zr0Vv-caoej-EWd9cpQVcYhA',
    arweaveOpenBitsCBVACPSTSource: 'ktzyKTMpH-HsLc8fuLcG2jzVO9V6mCFl4WC5lPWLRD8',
    OpenBitsFee: 0.01,
    installationFee: 0.01,
    publicationFee: 0.0001,
    userPublications: [],
    'arweave-key': null,
  });
  return openBitsArweaveConfig;
};

export default configStore;
