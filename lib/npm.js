import npm from 'npm';
import pack from 'libnpmpack';

export const installNodePackage = async (packageName, saveDev) => {
  return new Promise(resolve => {
    npm.load({
      loaded: false,
      'save-dev': saveDev,
    }, () => {
      npm.commands.install([packageName], cb => {
        return resolve(cb);
      });
    });
  });
};

export const packNodePackage = async () => {
  const tarball = await pack();
  return tarball;
};
