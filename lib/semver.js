import latestSemver from 'latest-semver';

const latestVersion = (versions) => {
  const latest = latestSemver(versions);
  return latest;
};

export default latestVersion;
